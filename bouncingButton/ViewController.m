//
//  ViewController.m
//  bouncingButton
//
//  Created by Engineer on 15/02/14.
//  Copyright (c) 2014 Engineer. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Ye Code hai bounce ke liye

-(IBAction)bounceButton{
    
    //Code Responsibile for the bpuncing effect
    
    CGPoint origin = self.button.center;
    CGPoint target = CGPointMake(self.button.center.x, self.button.center.y-16);
    CABasicAnimation *bounce = [CABasicAnimation animationWithKeyPath:@"position.y"];
    // bounce.duration = 0.9;
    bounce.fromValue = [NSNumber numberWithInt:origin.y];
    bounce.toValue = [NSNumber numberWithInt:target.y];
    bounce.repeatCount = 3.0;
    bounce.autoreverses = YES;
    bounce.speed = 0.9;
    bounce.cumulative =NO;
    [self.button.layer addAnimation:bounce forKey:@"position"];
   
  
}

@end
