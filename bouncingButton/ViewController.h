//
//  ViewController.h
//  bouncingButton
//
//  Created by Engineer on 15/02/14.
//  Copyright (c) 2014 Engineer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *button;
-(IBAction)bounceButton;
@end
